<?php

namespace BlogBundle\Command;

use BlogBundle\Entity\NewsTwitter;
use BlogBundle\Service\Utility;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class ParserTweetCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('twitter:parser')
            ->setDescription('get method')
            ->addOption('method', null, InputOption::VALUE_OPTIONAL, 'get method for parsing')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        switch($input->getOption('method')) {
            case 'getUserTimeline' :
                $this->getUserTimeline();
                break;
        }

    }

    private function getUserTimeline() {
        set_time_limit (500);
        $twitter = $this->getContainer()->get('endroid.twitter');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $response = $twitter->query('statuses/user_timeline', 'GET', 'json', array('screen_name' => 'rianru', 'count' => 70));
        $tweets = json_decode($response->getContent(), true);
        $IdTweetsInDB = $em->getRepository('BlogBundle:NewsTwitter')->getAllTweetId(date('Y-m-d', time()-432000));
        $TwitterIDFromDB = Utility::getOneDimensionalArray($IdTweetsInDB, 'idTwitter');
        if (is_array($tweets) && count($tweets) > 0 ) {
            foreach ($tweets as $tweet) {
                if (!in_array($tweet['id_str'], $TwitterIDFromDB)) {
                    $newTweet = new NewsTwitter();
                    $newTweet->setCreateDate(new \DateTime(date('Y-m-d H:i:s', strtotime($tweet['created_at']) )) );
                    $newTweet->setMessageText($tweet['text']);
                    $newTweet->setIdTwitter($tweet['id_str']);
                    $newTweet->setFavorite($tweet['favorite_count']);
                    $newTweet->setRetweet($tweet['retweet_count']);
                    if (isset($tweet['entities']['hashtags']) && count($tweet['entities']['hashtags']) > 0 ) {
                        $tags = Utility::checkHashTag($tweet['entities']['hashtags']);
                        $newTweet->setHashtags(implode(';', $tags));
                    }
                    $em->persist($newTweet);
                }
            }
            $em->flush();
        }

    }

}



