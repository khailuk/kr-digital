<?php

namespace BlogBundle\Controller;

use BlogBundle\Service\Utility;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class BlogController extends Controller
{
    const BLOGS_IN_PAGE = 20;

    public function getDateTanDaysAgo() {
        return date('Y-m-d' , time()-1730000);
    }

    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $blogs = $em->getRepository('BlogBundle:NewsTwitter')->getBlogs();
        $most_popular = $em->getRepository('BlogBundle:NewsTwitter')->getPopularNews(self::getDateTanDaysAgo());
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $blogs,
            $request->query->getInt('page', 1),
            self::BLOGS_IN_PAGE
        );
        return array('most_popular' => $most_popular, 'blogs' => $pagination, 'BLOGS_IN_PAGE' => self::BLOGS_IN_PAGE);
    }

    /**
     * @Route("/search", name="news_search")
     * @Template()
     */
    public function searchAction(Request $request)
    {
        $search = $request->get('text_search');
        $em = $this->getDoctrine()->getManager();
        $blogs = $em->getRepository('BlogBundle:NewsTwitter')->getBlogsSearch($search);
        $most_popular = $em->getRepository('BlogBundle:NewsTwitter')->getPopularBlogsWithSearch($search, self::getDateTanDaysAgo());
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $blogs,
            $request->query->getInt('page', 1),
            self::BLOGS_IN_PAGE
        );
        return array('most_popular' => $most_popular, 'search_text' => $search, 'blogs' => $pagination, 'BLOGS_IN_PAGE' => self::BLOGS_IN_PAGE);
    }

    /**
     * @Route("/{tag}", name="hashtag")
     * @Template()
     */
    public function hashTagNewsAction($tag, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $blogs = $em->getRepository('BlogBundle:NewsTwitter')->getBlogsByTags($tag);
        $most_popular = $em->getRepository('BlogBundle:NewsTwitter')->getPopularNews(self::getDateTanDaysAgo());
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $blogs,
            $request->query->getInt('page', 1),
            self::BLOGS_IN_PAGE
        );
        return array( 'most_popular' => $most_popular, 'tag' => $tag, 'blogs' => $pagination, 'BLOGS_IN_PAGE' => self::BLOGS_IN_PAGE );
    }




}
