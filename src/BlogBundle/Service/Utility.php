<?php

namespace BlogBundle\Service;


class Utility
{

    public static function getOneDimensionalArray(array $parserArray, $nameRow)
    {
        $OneDimensionalArrayForName = array();
        if (count($parserArray) > 0 ) {
            foreach ($parserArray as $key => $field) {
                $OneDimensionalArrayForName[$key] = $field[$nameRow];
            }
        }
        return $OneDimensionalArrayForName;
    }

    public static function checkHashTag(array $TwitterTags)
    {
        $tags = array();
        foreach ($TwitterTags as $hash_tag) {
            $tags[] = $hash_tag['text'];
        }
        return $tags;
    }

}