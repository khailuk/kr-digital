<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsTwitter
 *
 * @ORM\Table(name="news_twitter")
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\NewsTwitterRepository")
 */
class NewsTwitter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $createDate;

    /**
     * @var datetime
     *
     * @ORM\Column(name="hashtags", type="string", length=255, nullable=true)
     */
    private $hashtags;


    /**
     * @var string
     *
     * @ORM\Column(name="id_str", type="string", length=32)
     */
    private $idTwitter;

    /**
     * @var string
     *
     * @ORM\Column(name="message_text", type="string", length=255, nullable=true)
     */
    private $messageText;

    /**
     * @var string
     *
     * @ORM\Column(name="retweet_count", type="integer", nullable=true, options={"default" : "0"})
     */
    private $retweet;

    /**
     * @var string
     *
     * @ORM\Column(name="favorite_count", type="integer", nullable=true, options={"default" : "0"})
     */
    private $favorite;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->retweet = 0;
        $this->favorite = 0;
    }


    public function HashTagsAsArray() {
        if (!empty($this->hashtags) ) {
            return explode(';', $this->hashtags) ;
        }
        return null;
    }

    public function DateCreateNoHours() {
        if (!empty($this->createDate) ) {
            return $this->createDate->format('Y-m-d') ;
        }
        return null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return NewsTwitter
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set hashtags
     *
     * @param string $hashtags
     *
     * @return NewsTwitter
     */
    public function setHashtags($hashtags)
    {
        $this->hashtags = $hashtags;

        return $this;
    }

    /**
     * Get hashtags
     *
     * @return string
     */
    public function getHashtags()
    {
        return $this->hashtags;
    }

    /**
     * Set idTwitter
     *
     * @param integer $idTwitter
     *
     * @return NewsTwitter
     */
    public function setIdTwitter($idTwitter)
    {
        $this->idTwitter = $idTwitter;

        return $this;
    }

    /**
     * Get idTwitter
     *
     * @return integer
     */
    public function getIdTwitter()
    {
        return $this->idTwitter;
    }

    /**
     * Set messageText
     *
     * @param string $messageText
     *
     * @return NewsTwitter
     */
    public function setMessageText($messageText)
    {
        $this->messageText = $messageText;

        return $this;
    }

    /**
     * Get messageText
     *
     * @return string
     */
    public function getMessageText()
    {
        return $this->messageText;
    }

    /**
     * Set retweet
     *
     * @param integer $retweet
     *
     * @return NewsTwitter
     */
    public function setRetweet($retweet)
    {
        $this->retweet = $retweet;

        return $this;
    }

    /**
     * Get retweet
     *
     * @return integer
     */
    public function getRetweet()
    {
        return $this->retweet;
    }

    /**
     * Set favorite
     *
     * @param integer $favorite
     *
     * @return NewsTwitter
     */
    public function setFavorite($favorite)
    {
        $this->favorite = $favorite;

        return $this;
    }

    /**
     * Get favorite
     *
     * @return integer
     */
    public function getFavorite()
    {
        return $this->favorite;
    }

}
